package ejemplo;

public class ShapeFactory {
	final static int CIRCLE = 1;
	final static int RECTANGLE = 2;
	final static int SQUARE = 3;

	// usa getShape para obtener objeto de la figura solicitada
	public Shape getShape(int shapeType) {
		Shape shape = null;
		switch (shapeType) {
		case CIRCLE:
			shape = new Circle();
			break;
		case RECTANGLE:
			shape = new Rectangle();
			break;
		case SQUARE:
			shape = new Square();
			break;
		default:
			shape = null;
		}
		return shape;
	}
}
